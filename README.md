# GameDeals | TeleBot

Телеграм Бот показывает различные игровые предложения, скидки на игры в разных магазинах

## Contents

- [App Structure](#app-structure)
- [Technologies](#technologies)
- [How to launch application](#how-to-launch-application)
- [Example](#example)

## App Structure

- config_data - Содержит настройки приложения, вкл переменные окружения и команды
- database - Содержит модели ORM peewee
- handlers - содержит обработчики команд бота
- keyboards - содержит обработчики кнопок в приложении
- states - содержит состояния для команд бота
- utils - Содержит утилиты (функции-помошники)
- loader - Содержит код для загрузки бота, бд
- main - Основной файл запуска бота

## Technologies

- **TeleBot** - Фреймворк для создания бота
- **SQLLite** - Легковесная база данных для хранения истории запросов
- **Peewee ORM** - Удобная и простая в использовании ORM

## How to launch application

Создать .env файл на основе шаблона, например:

```
BOT_TOKEN = ''

Чтобы получить токен, вы можете связаться со мной 
или зарегисрировать свой от @BotFather

```

- `pip install -r requirements.txt` - Установка зависимостей
- `python main.py` - Запуск основного файла
- @gaming_deals_bot - ссылка на бота

## Example

![img.png](README_images/1.png)

![img.png](README_images/2.png)

![img.png](README_images/3.png)
