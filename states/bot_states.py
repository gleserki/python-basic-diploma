from telebot.handler_backends import State, StatesGroup


class LowPriceInfo(StatesGroup):
    store_name = State()
    deals_amt = State()


class HighPriceInfo(StatesGroup):
    store_name = State()
    deals_amt = State()


class CustomInfo(StatesGroup):
    game_name = State()
    min_price = State()
    max_price = State()
    deals_amt = State()
