"""
Различный функционал, который потребуется для реализации обработчиков
сообщений

"""

import requests
from typing import Iterator, Generator
import json


def get_stores() -> Iterator[dict]:
    """Итератор, перебирающий магазины из списка доступных

    Returns:
        Iterator[dict]
    """

    req = requests.get("https://www.cheapshark.com/api/1.0/stores")
    json_obj = json.loads(req.text)

    result = filter(lambda store: store["isActive"], json_obj)

    return result


def best_deals(store_id: int, amt: int) -> Generator[dict, None, None]:
    """Генератор, генерирующий лучшие предложения

    Args:
        store_id (int): Id магазина
        amt (int): Кол-во предложений_

    Yields:
        dict: Словарь с данными о предложении
    """

    req = requests.get(
        f"https://www.cheapshark.com/api/1.0/deals?pageSize={amt}&storeID={store_id}"
        "&sortBy=Deal Rating"
        "&AAA=1"
    )
    json_obj = json.loads(req.text)

    for item in json_obj:
        yield item


def worst_deals(store_id: int, amt: int) -> Generator[dict, None, None]:
    """Генератор, генерирующий худшие предложения

    Args:
        store_id (int): Id магазина
        amt (int): Кол-во предложений_

    Yields:
        dict: Словарь с данными о предложении
    """

    req = requests.get(
        f"https://www.cheapshark.com/api/1.0/deals",
        params={
            "storeID": store_id,
            "pageSize": amt,
            "lowerPrice": 29,
            "sortBy": "Deal Rating:asc"
        }
    )
    json_obj = json.loads(req.text)

    for item in json_obj:
        yield item


def custom_deals(game_name: str, low_price: int, high_price: int) -> Iterator[dict]:
    """Итератор, перебирающий пользовательские предложения

    Args:
        game_name (str): Название игры
        low_price (int): Минимальная цена
        high_price (int): Максимальная цена

    Returns:
        Iterator: Итератор, значение которго словарь с данным о предложении
    """

    req = requests.get(
        f"https://www.cheapshark.com/api/1.0/games",
        params={
            "title": game_name,
        }
    )

    json_obj = json.loads(req.text)

    result = filter(
        lambda deal: low_price <= float(deal["cheapest"]) <= high_price,
        json_obj
    )

    return result


def get_deal(deal_id: str) -> dict:
    """Получить словарь с данными о предложении

    Args:
        deal_id (str): ID предложения

    Returns:
        dict: Дынные о предложении
    """

    req = requests.get(
        f"https://www.cheapshark.com/api/1.0/deals?id={deal_id}"
    )

    deal = json.loads(req.text)

    return deal["gameInfo"]
