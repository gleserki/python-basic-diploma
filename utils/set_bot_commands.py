from telebot.types import BotCommand
from config_data.config import DEFAULT_COMMANDS, CUSTOM_COMMANDS


def set_commands(bot):
    "Устанавливает команды бота из DEFAUL_COMANDS и CUSTOM_COMANDS"
    bot.set_my_commands(
        [BotCommand(*i) for i in [*DEFAULT_COMMANDS, *CUSTOM_COMMANDS]]
    )
