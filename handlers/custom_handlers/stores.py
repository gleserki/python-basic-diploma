
from telebot.types import Message
from loader import bot, db
from utils.misc.functional import get_stores
from database.user_model import User
from datetime import datetime


@bot.message_handler(commands=["stores"])
def bot_stores(message: Message):
    """

    Показывает список магазинов, по которым будет идти поиск предложений
    """

    strings = [f"{'_' * 10}<b>\U0001F525Магазины</b>\U0001F525{'_' * 10}\n"]

    for i_store, store in enumerate(get_stores()):
        strings.append(f"{i_store + 1}) {store['storeName']}")

    text = "\n".join(strings)
    bot.send_message(message.from_user.id, text, parse_mode='HTML')

    date = datetime.now()
    with db:
        User.create(
            telegram_id=message.from_user.id,
            name=message.from_user.full_name,
            command="/stores",
            date_field=date.strftime("%d.%m.%Y"),
            time_field=date.strftime("%H : %M : %S")
        )
