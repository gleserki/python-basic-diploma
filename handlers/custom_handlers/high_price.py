
from telebot.types import Message
from loader import bot, db
from states.bot_states import HighPriceInfo
from utils.misc.functional import get_stores, worst_deals
from keyboards.inline.inline_buttons import deal_markup
from config_data.config import DEFAULT_COMMANDS, CUSTOM_COMMANDS
from database.user_model import User
from datetime import datetime


@bot.message_handler(commands=["highprice"],
                     func=lambda msg: bot.get_state(msg.from_user.id, msg.chat.id) is None)
def bot_high_price(message: Message) -> None:
    """
    Переводит пользователя в состояние запроса названия магазина
    """
    bot.set_state(message.from_user.id, HighPriceInfo.store_name, message.chat.id)
    text = "\n".join([
        "Напишите название магазина: ",
        "Cписок магазинов по которым я ищу, можно посмотреть используя команду /stores"
    ])
    bot.reply_to(message, text)

    date = datetime.now()
    with db:
        User.create(
            telegram_id=message.from_user.id,
            name=message.from_user.full_name,
            command="/highprice",
            date_field=date.strftime("%d.%m.%Y"),
            time_field=date.strftime("%H : %M : %S")
        )


@bot.message_handler(state=HighPriceInfo.store_name)
def get_store_name(message: Message) -> None:
    """

    Проверяет введенное название магазина на наличие ошибок и
    на существование в списке магазинов

    При отсутсвии ошибок переводит пользователя в
    состояния запроса кол-ва предложений для показа

    """

    try:
        if message.text.strip('/') in map(lambda command: command[0], [*DEFAULT_COMMANDS, *CUSTOM_COMMANDS]):
            raise ValueError(
                "Пожалуйста отмените текущую команду!",
                "Для отмены вы можете использовать /cansel"
            )

    except ValueError as exc:
        bot.reply_to(message, "\n".join(exc.args))
    else:

        for store in get_stores():
            if store['storeName'].lower() == message.text.lower():
                text = "\n".join([
                    "Отлично! Введите число",
                    "Кол-во предложений в пределах от 1 до 10"
                ])
                bot.send_message(message.from_user.id, text)
                bot.set_state(message.from_user.id, HighPriceInfo.deals_amt, message.chat.id)

                with bot.retrieve_data(message.from_user.id, message.chat.id) as data:
                    data['store_name'] = message.text
                    # Сохраняю также ID так как достасть предложения из API можно ток по ID
                    data['store_id'] = store['storeID']

                break
        else:
            text = "\n".join([
                "Извини! Но такого магазина нет в моём списке",
                "Используй команду /stores для того чтобы посмотреть список магазинов!"
            ])
            bot.reply_to(message, text)


@bot.message_handler(state=HighPriceInfo.deals_amt)
def get_deals_amt(message: Message) -> None:
    """

    Проверяет число на тип, и соответсвие диапозону

    В случае отсуствия ошибок выводит информацию о предложениях и
    переводит в состояние

    """
    try:
        if message.text.strip('/') in map(lambda command: command[0], [*DEFAULT_COMMANDS, *CUSTOM_COMMANDS]):
            raise ValueError(
                "Пожалуйста отмените текущую команду!",
                "Для отмены вы можете использовать /cansel"
            )

        if not message.text.isdigit():
            raise ValueError(
                "Пожалуйста введите число!",
                "Кол-во предложений в пределах от 1 до 10!"
            )
        else:
            amt = int(message.text)

        if not (1 <= amt <= 10):
            raise ValueError(
                "Пожалуйста введите число!",
                "Кол-во предложений в пределах от 1 до 10!"
            )
    except ValueError as exc:
        bot.reply_to(message, "\n".join(exc.args))
    else:

        with bot.retrieve_data(message.from_user.id, message.chat.id) as data:
            data['deals_amt'] = message.text
            for deal in worst_deals(int(data['store_id']), int(data['deals_amt'])):
                text = "\n".join([
                    f"Название - {deal['title']}",
                    f"\nТекущая цена - {deal['salePrice']}\U0001F4B0",
                    f"\nЦена при релизе - {deal['normalPrice']}\U0001F4B0",
                    f"\nРейтинг MetaCritic - {deal['metacriticScore']}/100",
                    f"\nРейтинг предложения - {deal['dealRating']} <b>\U00002744 COLD! \U00002744</b>"
                ])
                bot.send_photo(
                    message.from_user.id,
                    deal['thumb'],
                    caption=text,
                    parse_mode='HTML',
                    reply_markup=deal_markup(f"https://www.cheapshark.com/redirect?dealID={deal['dealID']}")
                )

        # Удаляю состояние
        bot.delete_state(message.from_user.id, message.chat.id)
