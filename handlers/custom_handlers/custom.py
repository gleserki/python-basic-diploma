
from telebot.types import Message
from loader import bot, db
from states.bot_states import CustomInfo
from utils.misc.functional import custom_deals, get_deal
from keyboards.inline.inline_buttons import deal_markup
from config_data.config import DEFAULT_COMMANDS, CUSTOM_COMMANDS
from database.user_model import User
from datetime import datetime


@bot.message_handler(commands=["custom"],
                     func=lambda msg: bot.get_state(msg.from_user.id, msg.chat.id) is None)
def bot_custom(message: Message) -> None:
    """
    Переводит пользователя в состояние запроса названия игры
    """

    bot.set_state(message.from_user.id, CustomInfo.game_name, message.chat.id)
    text = "\n".join([
            "Напишите пожалуйста название игры!!",
            "Рекомендую указать конкретное название игры, чтобы сузить поиск"
    ])
    bot.reply_to(message, text)

    date = datetime.now()
    with db:
        User.create(
            telegram_id=message.from_user.id,
            name=message.from_user.full_name,
            command="/custom",
            date_field=date.strftime("%d.%m.%Y"),
            time_field=date.strftime("%H : %M : %S")
        )


@bot.message_handler(state=CustomInfo.game_name)
def get_game_name(message: Message) -> None:
    """
    Получает имя введенное пользователем и
    переводит бота в состояние запроса минимальной цены

    """
    try:
        if message.text.strip('/') in map(lambda command: command[0], [*DEFAULT_COMMANDS, *CUSTOM_COMMANDS]):
            raise ValueError(
                "Пожалуйста отмените текущую команду!",
                "Для отмены вы можете использовать /cansel"
            )
    except ValueError as exc:
        bot.reply_to(message, "\n".join(exc.args))
    else:
        text = "\n".join([
            "Отлично! Напиши минимальную цену товара!",
            "В пределах от 0 до 50 $"
        ])
        bot.send_message(message.from_user.id, text)
        bot.set_state(message.from_user.id, CustomInfo.min_price, message.chat.id)

        with bot.retrieve_data(message.from_user.id, message.chat.id) as data:
            data['game_name'] = message.text


@bot.message_handler(state=CustomInfo.min_price)
def get_min_price(message: Message) -> None:
    """
    Проверяет минимальную цену на ошибки ограничения цены,
    а также является ли цена числом
    """
    try:

        if message.text.strip('/') in map(lambda command: command[0], [*DEFAULT_COMMANDS, *CUSTOM_COMMANDS]):
            raise ValueError(
                "Пожалуйста отмените текущую команду!",
                "Для отмены вы можете использовать /cansel"
            )

        if not message.text.isdigit():
            raise ValueError(
                "Пожалуйста введите число!",
                "Минимальную цену в пределах от 0 до 50 $!"
            )
        else:
            min_price = int(message.text)

        if not (0 <= min_price <= 50):
            raise ValueError(
                "Пожалуйста введите число!",
                "Минимальную цену в пределах от 0 до 50 $!"
            )

    except ValueError as exc:
        bot.reply_to(message, "\n".join(exc.args))
    else:

        text = "\n".join([
            "Отлично! Напиши максимальную цену товара!",
            "В пределах от 0 до 50 $"
        ])
        bot.send_message(message.from_user.id, text)
        bot.set_state(message.from_user.id, CustomInfo.max_price, message.chat.id)

        with bot.retrieve_data(message.from_user.id, message.chat.id) as data:
            data['min_price'] = min_price


@bot.message_handler(state=CustomInfo.max_price)
def get_max_price(message: Message) -> None:
    """
    Проверяет максимальную цену на ошибки ограничения цены,
    является ли цена числом, а также меньше ли она минимальной цены

    """
    try:
        if message.text.strip('/') in map(lambda command: command[0], [*DEFAULT_COMMANDS, *CUSTOM_COMMANDS]):
            raise ValueError(
                "Пожалуйста отмените текущую команду!",
                "Для отмены вы можете использовать /cansel"
            )

        if not message.text.isdigit():
            raise ValueError(
                "Пожалуйста введите число!",
                "Максимальную цену в пределах от 0 до 50 $!"
            )
        else:
            max_price = int(message.text)

        if not (0 <= max_price <= 50):
            raise ValueError(
                "Пожалуйста введите число!",
                "Максимальную цену в пределах от 0 до 50 $!"
            )

        with bot.retrieve_data(message.from_user.id, message.chat.id) as data:
            if max_price < data['min_price']:
                raise ValueError(
                    "Пожалуйста исправьте максимальную цену!",
                    "Максимальная цена не может быть меньше минимальной"
                )
            else:
                data['max_price'] = max_price

    except ValueError as exc:
        bot.reply_to(message, "\n".join(exc.args))
    else:
        text = "\n".join([
            "Отлично! Введите число",
            "Кол-во предложений в пределах от 1 до 10 "
        ])
        bot.send_message(message.from_user.id, text)
        bot.set_state(message.from_user.id, CustomInfo.deals_amt, message.chat.id)


@bot.message_handler(state=CustomInfo.deals_amt)
def get_deals_amt(message: Message) -> None:
    """
    Обрабатывает кол-во предложений на различные ошибки
    Выводит в качестве результата список предложений.
    Если допустимого кол-ва не было найдено или по ццене,то выводит соотвествующие сообщения
    """
    try:
        if message.text.strip('/') in map(lambda command: command[0], [*DEFAULT_COMMANDS, *CUSTOM_COMMANDS]):
            raise ValueError(
                "Пожалуйста отмените текущую команду!",
                "Для отмены вы можете использовать /cansel"
            )

        if not message.text.isdigit():
            raise ValueError(
                "Пожалуйста введите число!",
                "Кол-во предложений в пределах от 1 до 10!"
            )
        else:
            deals_amt = int(message.text)

        if not (1 <= deals_amt <= 10):
            raise ValueError(
                "Пожалуйста введите число!",
                "Кол-во предложений в пределах от 1 до 10!"
            )
    except ValueError as exc:
        bot.reply_to(message, "\n".join(exc.args))
    else:

        with bot.retrieve_data(message.from_user.id, message.chat.id) as data:
            data['deals_amt'] = deals_amt
            for i_deal, deal in enumerate(custom_deals(
                data['game_name'],
                data['min_price'],
                data['max_price']
            )):
                # В случае если собираемся показать кол-во предложений больше чем необходимо
                if (i_deal + 1) > deals_amt:
                    break

                game_deal = get_deal(deal['cheapestDealID'])

                text = "\n".join([
                    f"Название - {game_deal['name']}",
                    f"\nТекущая цена - {game_deal['salePrice']}\U0001F4B0",
                    f"\nЦена при релизе - {game_deal['retailPrice']}\U0001F4B0",
                    f"\nРейтинг MetaCritic - {game_deal['metacriticScore']}/100"
                ])
                bot.send_photo(
                    message.from_user.id,
                    game_deal['thumb'],
                    caption=text,
                    parse_mode='HTML',
                    reply_markup=deal_markup(f"https://www.cheapshark.com/redirect?dealID={deal['cheapestDealID']}")
                )

            # В случае если предложений не было найдено
            if "i_deal" not in locals():
                text = "\n".join([
                    "Извините, я не нашел предложений",
                    "по параметрам которые вы указали"
                ])
                bot.send_message(message.from_user.id, text)
            # В случае если, кол-во найденных предложений меньше запрашиваемого кол-во пользователем
            elif (i_deal + 1) < deals_amt:
                text = "\n".join([
                    "Извините, это всё что мне удалось найти,",
                    "по параметрам которые вы указали"
                ])
                bot.send_message(message.from_user.id, text)

        # Удаляю состояние
        bot.delete_state(message.from_user.id, message.chat.id)
