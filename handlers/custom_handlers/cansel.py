from telebot.types import Message

from loader import bot, db
from database.user_model import User
from datetime import datetime


@bot.message_handler(commands=["cansel"])
def bot_cansel(message: Message) -> None:
    current_state = bot.current_states.get_state(message.chat.id, message.from_user.id)
    if current_state is not None:
        bot.reply_to(message, "Текущая команда отменена!")
        bot.delete_state(message.from_user.id, message.chat.id)
    else:
        bot.reply_to(message, "Сейчас у вас ни одна из команд не запущена")

    date = datetime.now()
    with db:
        User.create(
            telegram_id=message.from_user.id,
            name=message.from_user.full_name,
            command="/cansel",
            date_field=date.strftime("%d.%m.%Y"),
            time_field=date.strftime("%H : %M : %S")
        )
