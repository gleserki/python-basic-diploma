from telebot.types import Message

from config_data.config import DEFAULT_COMMANDS, CUSTOM_COMMANDS
from loader import bot, db
from database.user_model import User
from datetime import datetime


@bot.message_handler(commands=["help"])
def bot_help(message: Message):
    """

    Показывает список доступных команд
    """
    text = [f"/{command} - {desk}" for command, desk in [*DEFAULT_COMMANDS, *CUSTOM_COMMANDS]]
    bot.reply_to(message, "\n".join(text))

    date = datetime.now()
    with db:
        User.create(
            telegram_id=message.from_user.id,
            name=message.from_user.full_name,
            command="/help",
            date_field=date.strftime("%d.%m.%Y"),
            time_field=date.strftime("%H : %M : %S")
        )
