from telebot.types import Message

from loader import bot, db
from database.user_model import User
from datetime import datetime

# Эхо хендлер, куда летят текстовые сообщения без указанного состояния
@bot.message_handler(commands=["history"])
def bot_history(message: Message):
    """
    Показывает историю запросов
    """
    date = datetime.now()
    with db:
        User.create(
            telegram_id=message.from_user.id,
            name=message.from_user.full_name,
            command="/history",
            date_field=date.strftime("%d.%m.%Y"),
            time_field=date.strftime("%H : %M : %S")
        )

    with db:
        records = User.select().where(User.telegram_id == message.from_user.id)

    text = [f"{'_' * 10}<b>\U00002B07История запросов</b>\U00002B07{'_' * 10}\n"]
    for i_record, user in enumerate(records):
        text.append(
            f"{i_record + 1}) {user.name}\n"
            f"         Команда: {user.command}\n"
            f"         Дата: {user.date_field}\n"
            f"         Время: {user.time_field}\n"
        )

    bot.send_message(message.from_user.id, "\n".join(text), parse_mode='HTML')
