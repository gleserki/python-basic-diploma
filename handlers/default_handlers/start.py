from telebot.types import Message
from config_data.config import DEFAULT_COMMANDS, CUSTOM_COMMANDS
from loader import bot


@bot.message_handler(commands=["start"])
def bot_start(message: Message):
    """
    Запускает бота
    """

    comands = [f"/{command} - {desk}" for command, desk in [*DEFAULT_COMMANDS, *CUSTOM_COMMANDS]]

    text = "\n".join([
        f"Привет, {message.from_user.full_name}!",
        "Я помогу подобрать игровые предложение, вот мои команды:\n",
        "\n".join(comands)
    ])

    bot.reply_to(message, text)
