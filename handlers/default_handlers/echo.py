from telebot.types import Message

from loader import bot


# Эхо хендлер, куда летят текстовые сообщения без указанного состояния
@bot.message_handler(state=None)
def bot_echo(message: Message):

    text = "\n".join([
        "Неизвестная команда!!!",
        "используйте /help чтобы посмотреть команды"
    ])

    bot.reply_to(
        message, text
    )
