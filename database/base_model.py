from peewee import *
from loader import db


class BaseModel(Model):
    class Meta:
        database = db
