from .base_model import BaseModel
from peewee import *


class User(BaseModel):
    telegram_id = IntegerField(null=False)
    name = CharField(50, null=False)
    command = CharField(50, null=False)
    date_field = DateField()
    time_field = TimeField(null=False)
