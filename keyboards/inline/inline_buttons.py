from telebot.types import InlineKeyboardButton, InlineKeyboardMarkup


def deal_markup(game_deal_url: str):
    """Добавляет кнопку с ссылкой в InlineKeyboardMarkup

    Args:
        game_deal_url (str): Ссылка на предложение с игрой

    Returns:
        InlineKeyboardMarkup: перечень кнопок
    """
    markup = InlineKeyboardMarkup()
    markup.add(InlineKeyboardButton(
        "Перейти на страницу предложения", url=game_deal_url, callback_data="Link"
    ))
    return markup
