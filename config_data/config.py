import os
from dotenv import load_dotenv, find_dotenv

if not find_dotenv():
    exit("Переменные окружения не загружены т.к отсутствует файл .env")
else:
    load_dotenv()

BOT_TOKEN = os.getenv("BOT_TOKEN")

DEFAULT_COMMANDS = (
    ("start", "Запустить бота"),
    ("help", "Вывести справку"),
    ("history", "История запросов"),
    ("cansel", "Отменить поиск предложений")
)

CUSTOM_COMMANDS = (

    ("stores", "Cписок магазинов"),
    ("lowprice", "Помогу подобрать дешевые предложения"),
    ("highprice", "Помогу подобрать дорогие предложения"),
    ("custom", "Помогу подобрать дешевые предложения, по названию игры")

)
