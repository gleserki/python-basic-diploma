from telebot import TeleBot
from telebot.storage import StateMemoryStorage
from config_data import config
from peewee import *

db = SqliteDatabase('history.db')
storage = StateMemoryStorage()
bot = TeleBot(token=config.BOT_TOKEN, state_storage=storage)
